# Open the same tmux session or create it if it does not exist
# This script is intended to be used as part of users' .profile/.bashrc

#tmux session name, the username by default
SESSION_NAME=`whoami`

#Find the tmux session, silently
tmux has-session -t "$SESSION_NAME" 2>/dev/null
if [ $? -eq 0 ]
then
  #Session exists, check if this script is running within tmux
  if [ -z "$TMUX" ]
  then
    #Is NOT a nested session, attach to it
    tmux attach-session -t "$SESSION_NAME"
  else
    #It is a nested session, we are already within tmux
    #Don't make a nested attach

    #Run a command just for newly created tmux sessions
    #replace with 'true' if you don't want anything to show
    echo 'Hello Tmux!'
  fi
else
  #Session does not exist, create it
  tmux new-session -s "$SESSION_NAME"
fi
