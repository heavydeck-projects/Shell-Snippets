#!/bin/bash

# Makes a 5x4 grid of movie thumbnails that fits on a 1280x720 image
#
# Requires:
#   - netpbm (pamscale, pnmcat, pnmtojpeg)
#   - ffmpeg (ffprobe, ffmpeg)
#

FILE="$1"
XFIT=1280
YFIT=720

echo -ne "Procesing $FILE... "
RATE=`ffprobe -v 0 -of csv=p=0 -select_streams v:0 -show_entries stream=r_frame_rate "$FILE"`
RATE=`echo "$RATE" | sed 's|/| / |'`
FRAMES=`ffprobe -v error -select_streams v:0 -count_packets -show_entries stream=nb_read_packets -of csv=p=0 "$FILE"`
echo $FRAMES frames @ $RATE

for i in {1..20}
do

	TIMECODE=`expr '(' $i '*' $FRAMES / 21 ')' / '(' $RATE ')'`
	rm -f "$FILE-$i.ppm"
	ffmpeg -ss $TIMECODE -i "$FILE" -vframes 1 -q:v 2 "$FILE-$i.ppm" 2>/dev/null
done

#Make the 4 rows
pnmcat -leftright "$FILE-1.ppm" "$FILE-2.ppm" "$FILE-3.ppm" "$FILE-4.ppm" "$FILE-5.ppm" > "$FILE-ROW-1.ppm"
pnmcat -leftright "$FILE-6.ppm" "$FILE-7.ppm" "$FILE-8.ppm" "$FILE-9.ppm" "$FILE-10.ppm" > "$FILE-ROW-2.ppm"
pnmcat -leftright "$FILE-11.ppm" "$FILE-12.ppm" "$FILE-13.ppm" "$FILE-14.ppm" "$FILE-15.ppm" > "$FILE-ROW-3.ppm"
pnmcat -leftright "$FILE-16.ppm" "$FILE-17.ppm" "$FILE-18.ppm" "$FILE-19.ppm" "$FILE-20.ppm" > "$FILE-ROW-4.ppm"

#Make full picture
pnmcat -topbottom "$FILE-ROW-1.ppm" "$FILE-ROW-2.ppm" "$FILE-ROW-3.ppm" "$FILE-ROW-4.ppm" > "$FILE-MOSAIC.ppm"

#Scale down
pamscale.exe -xyfit $XFIT $YFIT "$FILE-MOSAIC.ppm" | pnmtojpeg > "$FILE.jpg"

#delete stuff
rm -f "$FILE-1.ppm" "$FILE-2.ppm" "$FILE-3.ppm" "$FILE-4.ppm" "$FILE-5.ppm" "$FILE-6.ppm" "$FILE-7.ppm" "$FILE-8.ppm" "$FILE-9.ppm" "$FILE-10.ppm"
rm -f "$FILE-11.ppm" "$FILE-12.ppm" "$FILE-13.ppm" "$FILE-14.ppm" "$FILE-15.ppm" "$FILE-16.ppm" "$FILE-17.ppm" "$FILE-18.ppm" "$FILE-19.ppm" "$FILE-20.ppm"
rm -f "$FILE-ROW-1.ppm" "$FILE-ROW-2.ppm" "$FILE-ROW-3.ppm" "$FILE-ROW-4.ppm"
rm -f "$FILE-MOSAIC.ppm"
