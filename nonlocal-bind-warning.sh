#!/bin/bash

# Check all bindings have a valid local address.
#
# This script will check all the listening TCP socket bindings against all the
# network interface addresses and try to find a match. If any binding is found
# not to have a matching interface address, this script will return a non zero
# value equal to the number of invalid socket bindings.
# It will return 0 if all bindings have local addresses.

INVALID_COUNT=0
#For each listening TCP socket binding
for BINDING in $(ss --numeric --listen --tcp | awk '{print $4}' | grep ':')
do
  #Get address by trimming port <:> scope <%> and ipv6 brackets
  BOUND_ADDRESS=$(echo "$BINDING" | sed 's/:[^:]*$//' | sed 's/%[^%]*$//' | tr -d '[]')

  #If bound to <any> address, binding is valid
  if   [ "$BOUND_ADDRESS" = '::' ]
  then
    continue
  elif [ "$BOUND_ADDRESS" = '0.0.0.0' ]
  then
    continue
  elif [ "$BOUND_ADDRESS" = '*' ]
    continue
  fi

  #If bound to localhost presume it is valid
  if [ "$BOUND_ADDRESS" = '::1' ]
  then
    continue
  elif [[ "$BOUND_ADDRESS" == 127.* ]]
  then
    continue
  fi

  BINDING_VALID=0
  #For each interface address (yay! N*M complexity!)
  for IF_ADDRESS in $(ip -json addr show | jq .[].addr_info[].local - | tr -d '"')
  do
    #If any of the interface addresses match, the binding is valid
    if [ "$BOUND_ADDRESS" = "$IF_ADDRESS" ]
    then
      BINDING_VALID=1
    fi
  done

  #If by this point BINDING_VALID is still zero, increment the INVALID_COUNT
  if [ "$BINDING_VALID" -eq '0' ]
  then
    echo "Binding <$BINDING> is not valid"
    INVALID_COUNT=$(expr "$INVALID_COUNT" + 1)
  else
    echo "Binding <$BINDING> OK"
  fi
done

if [ "$INVALID_COUNT" -eq '0' ]
then
  echo "All OK!"
else
  echo "Warning! Some bindings are not valid."
fi

exit "$INVALID_COUNT"
