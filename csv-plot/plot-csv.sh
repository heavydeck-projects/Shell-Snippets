#!/bin/bash

IN_ODS=table-0000.ods
IN_CSV=$(echo "$IN_ODS" | sed 's|.ods|.csv|')

#Convert Libreoffice to CSV
rm -f "$IN_CSV"
libreoffice --headless --convert-to csv "$IN_ODS"

#Remove padding lines (all commas)
sed -i '/^,*$/d' "$IN_CSV"

#Copy generated csv to r_in.csv
cp "$IN_CSV" "r_in.csv"

#Plot the graph with R
./plot.r

#Make the negative version of it
#essentially swap black/white colors
cat out-pos.svg \
  | sed 's|rgb(100%,100%,100%)|rgb(1%,1%,1%)|g' \
  | sed 's|rgb(0%,0%,0%)|rgb(100%,100%,100%)|g' \
  | sed 's|rgb(1%,1%,1%)|rgb(0%,0%,0%)|g' \
    > out-neg.svg

#Cleanup
rm "$IN_CSV" "r_in.csv"
