#!/usr/bin/env Rscript
data <- read.csv('r_in.csv')

#Colors & stuff
#month_colors <- topo.colors(13)
month_colors <- rainbow(13)
line_width <- 4

#Extract names for series and chart
name <- colnames(data)[1]
months <- colnames(data)[2:13]

svg(filename="out-pos.svg", width = 12.4, height = 7)
x <- 1:31
plot(x, x, type = "n", xlim = c(1, 31), ylim = c(60, 80), ylab = "", xlab = "", main = "")
grid(NA, NULL, lwd = 1.5)

color_index = 0
for (month in months) {
    color_index = color_index + 1
    y <- data[[month]]
    x <- 1:length(y)
    if(length(y) != 0){
        lines(x, y, type = "l", col = month_colors[color_index], lwd = line_width)
    }
}

legend("top", legend = months, col = month_colors, ncol=6, lty = 1, lwd = line_width)
